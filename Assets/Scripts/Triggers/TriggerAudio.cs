﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TriggerAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip clip;

    [SerializeField]
    private SoundPlay soundPlay;

    [SerializeField]
    private List<Transform> transformsTarget;

    [SerializeField]
    private bool loop;

    [SerializeField]
    private float delay;

    [SerializeField]
    private TimerCount interval;

    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if (transformsTarget.Any(t=>t==other.transform))
        {
            if (interval.TimerOk())
                StartCoroutine(soundPlay.CorPlay(clip, loop, delay));
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (transformsTarget.Any(t => t == other))
        {
            if (this.loop)
            {
                StartCoroutine(soundPlay.CorStop(0.1f));
            }

        }
    }



}

