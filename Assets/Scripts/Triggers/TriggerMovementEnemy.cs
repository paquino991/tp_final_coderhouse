﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMovementEnemy : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        Enemy movementEnemy = other.gameObject.GetComponent<Enemy>();
        if (movementEnemy != null)
        {
            movementEnemy.ChangeMovementEnemy(MovementNpcEnum.WALK);
        }
    }

    //void OnTriggerExit(Collider other)
    //{
    //    if (transformsTarget.Any(t => t == other))
    //    {
    //        if (this.loop)
    //        {
    //            StartCoroutine(soundPlay.CorStop(0.1f));
    //        }

    //    }
    //}

}
