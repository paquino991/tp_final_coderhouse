﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class Clock : MonoBehaviour
    {
        private static Clock _instance { get; set; }
        public static Clock instance { get { return _instance; } }

        [SerializeField]
        private Text dateTime;
        // Start is called before the first frame update
        void Start()
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }

        public void SetDateTime(string strDateTime)
        {
            this.dateTime.text = strDateTime;
        }
        
        // Update is called once per frame
        void Update()
        {

        }
    }
}
