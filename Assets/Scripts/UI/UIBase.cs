﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIBase : MonoBehaviour
{
    [SerializeField]
    private Canvas canvas;
    public bool Visible { get { return this.visible; } }

    private bool visible;

    public virtual void Show()
    {
        visible = true;
        canvas.enabled = true;
    }

    public virtual void Hide()
    {
        visible = false;
        canvas.enabled = false;
    }
}
