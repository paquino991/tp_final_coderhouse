﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenu : UIBase
{
    private static UIMenu _instance { get; set; }
    public static UIMenu instance { get { return _instance; } }

    [SerializeField]
    private Button btResume;

    [SerializeField]
    private Button btExit;

    // Start is called before the first frame update
    void Start()
    {
        _instance = this;
        DontDestroyOnLoad(this);
        btResume.onClick.AddListener(Hide);
        btExit.onClick.AddListener(ManagerGame.Instance.QuitGame);
    }


    // Update is called once per frame
    void Update()
    {

    }

    public override void Show()
    {
        base.Show();
        ManagerGame.Instance.OnPaused();
    }

    public override void Hide()
    {
        base.Hide();
        ManagerGame.Instance.OnResume();
    }
}
