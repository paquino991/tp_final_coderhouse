﻿using UnityEngine;

public class ItemData : ScriptableObject
{
    [SerializeField]
    private string name;
    public string Name { get { return name; } } 
}


