﻿using UnityEngine;

[CreateAssetMenu(menuName= "Data/Items/LatternData")]
public class LatternData : ItemData
{
    [SerializeField]
    private float lightMax;
    [SerializeField]
    private float lightMin;
    [SerializeField]
    private float timeMax;

    public float LightMax { get { return lightMax; } }
    public float LightMin { get { return lightMin; } }
    public float TimeMax { get { return timeMax; } }
}


