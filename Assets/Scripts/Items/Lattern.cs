﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Lattern : ItemEquip
{
    [SerializeField]
    private LatternData data;

    //[SerializeField]
    //private float lightMax;

    //[SerializeField]
    //private float lightMin;

    [SerializeField]
    private float time;

    //[SerializeField]
    //private float timeMax;

    [SerializeField]
    private List<LightEffect> lights;

    [SerializeField]
    private TimerCount timer;

    private bool isTurnOn;

    public override void Desequiped(PlayerController player)
    {
        player.GetAnimation().UnEquip();
    }

    public override void Use(PlayerController player)
    {
        if (player.itemEquip != null)
        {
            player.GetAnimation().UnEquip();
            Destroy(player.itemEquip.gameObject);
            return;
        }

        var gm = Instantiate(this, player.transformItemEquip);
        gm.transform.parent = player.transformItemEquip;
        player.itemEquip = gm.GetComponent<Lattern>();
        player.GetAnimation().Equip();
    }

    public void AddOil(float quantity)
    {
        this.time += quantity;

        if (this.time > this.data.TimeMax)
            this.time = this.data.TimeMax;

        UpdateLight();
    }

    private void ConsumeOil()
    {
        this.time -= 1;
    }

    private void UpdateLight()
    {
        if (!isTurnOn)
            return;

        float forceLight = this.time / this.data.TimeMax;
        if (forceLight < 0.15f)
            forceLight = 0.15f;

        lights.ForEach(l => {
            l.max = this.data.LightMax * forceLight;
            l.min = this.data.LightMin * forceLight;
        });

    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TurnOn());
    }

    // Update is called once per frame
    void Update()
    {
        if (this.timer.TimerOk())
        {
            ConsumeOil();
            UpdateLight();
        }
    }

    private IEnumerator TurnOn()
    {
        int count = 0;

        while (count < 20)
        {
            float forceLight = this.time / this.data.TimeMax * 0.05f * count;

            lights.ForEach(l => {
                l.max = this.data.LightMax * forceLight;
                l.min = this.data.LightMin * forceLight;
            });

            count++;

            yield return new WaitForSeconds(0.01f);
        }

        isTurnOn = true;
    }

    private IEnumerator TurnOff(PlayerController player)
    {
        int count = 20;

        while (count > 0)
        {
            float forceLight = this.time / this.data.TimeMax * 0.05f * count;

            lights.ForEach(l => {
                l.max = this.data.LightMax * forceLight;
                l.min = this.data.LightMin * forceLight;
            });

            count--;

            yield return new WaitForSeconds(0.01f);
        }

        player.GetAnimation().UnEquip();
        Destroy(player.itemEquip.gameObject);

        isTurnOn = true;
    }
}
