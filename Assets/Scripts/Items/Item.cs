﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public abstract void Use(PlayerController playerController);
}

public abstract class ItemEquip : Item
{
    public abstract void Desequiped(PlayerController playerController);
}


