﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGame
{
    private static ManagerGame _Instance { get; set; }

    public static ManagerGame Instance {
        get
        {
            if (_Instance == null)
                _Instance = new ManagerGame();

            return _Instance;
        }
    }

    public bool IsPaused { get { return isPaused; } }
    private bool isPaused { get; set; }

    public void OnPaused() => this.isPaused = true;

    public void OnResume() => this.isPaused = false;

    public void QuitGame()
    {
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }

}
