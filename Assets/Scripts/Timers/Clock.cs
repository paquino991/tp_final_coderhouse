﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Clock : TimerAction
{
    private DateTime dateTime;

    [SerializeField]
    public UnityEvent<DateTime> OnUpdateUIClock;

    // Start is called before the first frame update
    void Start()
    {
        this.dateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
    }

    public override void Run()
    {
        AddSecond();
    }

    private void AddSecond()
    {
        dateTime = this.dateTime.AddSeconds(60);
        UpdateUIClock();

    }

    private void UpdateUIClock()
    {
        OnUpdateUIClock?.Invoke(this.dateTime);
        //Assets.Scripts.UI.Clock.instance.SetDateTime(this.dateTime.ToString("HH:mm") + " AM");
    }
}
