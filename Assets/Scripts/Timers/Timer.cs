﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private TimerAction action;

    [SerializeField]
    public float timeActionLimit;

    public float timeAction;

    private void Start()
    {
        this.timeAction = timeActionLimit;
    }

    void Update()
    {
        if (ManagerGame.Instance.IsPaused)
            return;

        UpdateInterval();
    }

    private void ResetTimer()
    {
        this.timeAction = timeActionLimit;
    }

    private void UpdateInterval()
    {
        this.timeAction -= Time.deltaTime;

        if (this.timeAction <= 0)
        {
            ResetTimer();
            action.Run();
        }
            

    }

}
