﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TimerAction : MonoBehaviour
{
    public abstract void Run();
}
