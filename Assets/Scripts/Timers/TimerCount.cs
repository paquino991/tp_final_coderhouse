﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerCount : MonoBehaviour
{

    [SerializeField]
    private float timeActionLimit;

    private float timeAction;

    private void Start()
    {
        this.timeAction = timeActionLimit;
    }

    void Update()
    {
        UpdateInterval();
    }

    private void ResetTimer()
    {
        this.timeAction = timeActionLimit;
    }

    private void UpdateInterval()
    {
        if (this.timeAction > 0)
            this.timeAction -= Time.deltaTime;
        else
            this.timeAction = 0;
    }

    public bool TimerOk()
    {
        if (timeAction == 0)
        {
            ResetTimer();
            return true;
        }

        return false;
    }
}
