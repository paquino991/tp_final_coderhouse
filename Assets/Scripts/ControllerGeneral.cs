﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerGeneral : MonoBehaviour
{
    [SerializeField]
    private TimerCount timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OpenMenu();
    }

    private void OpenMenu()
    {
        if (ManagerKeyboard.Instance.IsKeyPress(KeyboardPlayerEnum.OpenMenu) && timer.TimerOk())
        {
            if (ManagerGame.Instance.IsPaused)
                UIMenu.instance.Hide();
            else
                UIMenu.instance.Show();
        }
            
    }
}
