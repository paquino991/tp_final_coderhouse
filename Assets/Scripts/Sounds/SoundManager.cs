﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sound
{
    CRY_MAN = 1,
    CRY_WOMAN = 2,
    CRY_WOMAN2 = 3,
    LAUNGHTER = 4,
    ENERGY_DOWN = 5
}

public class SoundManager : MonoBehaviour
{
    private static SoundManager _Instance { get; set; }

    public static SoundManager Instance { get
        {
            return _Instance;
        } }


    [SerializeField]
    private AudioClip[] audios;
    // Start is called before the first frame update

    void Start()
    {
        _Instance = this;
    }

    public AudioClip Get(Sound sound)
    {
        if (audios.Length < (short)sound || audios[(short)sound] == null)
            throw new Exception("Sonido no encontrado");

        return audios[(short)sound];
    }
}
