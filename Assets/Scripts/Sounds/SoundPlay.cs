﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlay : MonoBehaviour
{
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.gameObject.GetComponent<AudioSource>();

        if (audioSource == null)
            Debug.LogError($"Audio {this.name} no encontrado");
    }


    public void Play(Sound sound, bool loop)
    {
        Play(SoundManager.Instance.Get(sound), loop);
    }


    public IEnumerator CorPlay(AudioClip clip, bool loop, float delay)
    {
        yield return new WaitForSeconds(delay);
        Play(clip, loop);
    }
    public void Play(AudioClip clip, bool loop)
    {
        audioSource.clip = clip;
        audioSource.loop = loop;
        audioSource.Play();
    }

    public void Stop()
    {
        this.audioSource.Stop();
    }

    public IEnumerator CorStop(float velocity)
    {
        while (audioSource.volume > 0.05f)
        {
            audioSource.volume -= 0.05f;
            yield return new WaitForSeconds(velocity);
        }

        audioSource.Stop();
        audioSource.volume = 1;

    }
}
