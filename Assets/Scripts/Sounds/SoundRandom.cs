﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundRandom : TimerAction
{
    [SerializeField]
    private Transform target;

    [SerializeField]
    private int distanceMax;

    [SerializeField]
    private int distanceMin;
    [SerializeField]
    private AudioClip[] audios;

    [SerializeField]
    private float duration;
    public override void Run()
    {
        PlaySoundRandom();
    }

    // Start is called before the first frame update
    private void PlaySoundRandom()
    {
        var clip = audios[Random.Range(0, audios.Length)];
        if (clip == null)
            throw new System.Exception("Audio no encontrado");

        var gm = GameObject.CreatePrimitive(PrimitiveType.Cube);
        gm.name = clip.name;
        gm.GetComponent<MeshRenderer>().enabled = false;
        gm.transform.position = target.position.RandomVector(distanceMin, distanceMax);
        AudioSource audio = gm.AddComponent<AudioSource>();
        gm.transform.parent = this.transform;
        audio.spatialBlend = 1;
        audio.clip = clip;
        //audio.minDistance = (int)(distanceMin * 0.01);
        //audio.maxDistance = (int)(distanceMax * 0.01);
        audio.Play();
        Destroy(gm, duration);
    }
}
