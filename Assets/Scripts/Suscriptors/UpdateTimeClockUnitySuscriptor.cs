﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateTimeClockUnitySuscriptor : MonoBehaviour
{
    public void Message(DateTime dateTime)
    {
        Debug.Log($"Enviado desde Timer a UpdateTimeClockUnitySuscriptor con hora {dateTime.ToString("HH:mm") + " AM"}");
    }

    public void UpdateUIClock(DateTime dateTime)
    {
        Assets.Scripts.UI.Clock.instance.SetDateTime(dateTime.ToString("HH:mm") + " AM");
    }
}

