﻿using UnityEngine;

public class RotatePlayerSuscriptor : MonoBehaviour
{
    private void Start()
    {
        PlayerController.OnRotate += MessageMove;
    }
    public void MessageMove()
    {
        Debug.Log($"Enviado desde PlayerController a RotatePlayerSuscriptor");
    }
}
