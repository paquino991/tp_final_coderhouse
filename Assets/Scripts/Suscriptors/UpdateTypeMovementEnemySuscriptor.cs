﻿using UnityEngine;

public class UpdateTypeMovementEnemySuscriptor : MonoBehaviour
{

    [SerializeField]
    private Enemy enemy;
    private void Start()
    {
        enemy.OnUpdateMovementType += MessageMove;
    }
    public void MessageMove()
    {
        Debug.Log($"Enviado desde Enemy a UpdateTypeMovementEnemySuscriptor");
    }
}
