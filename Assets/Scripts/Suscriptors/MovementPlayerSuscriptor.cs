﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayerSuscriptor : MonoBehaviour
{
    private void Start()
    {
        PlayerController.OnMovement += MessageMove;
    }
    public void MessageMove()
    {
        Debug.Log($"Enviado desde PlayerController a MovementPlayerSuscriptor");
    }
}
