﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCastPlayer : MonoBehaviour
{
    [SerializeField]
    private Transform vision;

    [SerializeField]
    private float radio;

    [SerializeField]
    private float distanceMax;

    [SerializeField]
    private LayerMask layers;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CreateRay();
    }

    private void CreateRay()
    {
        if (!Physics.SphereCast(vision.position, radio, vision.forward, out RaycastHit raycastHit, distanceMax, layers))
            return;

        if (raycastHit.rigidbody == null)
            return;
        var targetRay = raycastHit.collider.GetComponent<ITargetRayPlayer>();

        if (targetRay == null)
            return;

        targetRay.ExecuteAction(raycastHit.distance);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(vision.position, vision.forward * distanceMax);
    }
}
