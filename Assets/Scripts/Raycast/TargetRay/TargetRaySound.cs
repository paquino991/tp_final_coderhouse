﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetRaySound : MonoBehaviour, ITargetRayPlayer
{
    [SerializeField]
    private AudioClip clip;

    [SerializeField]
    private SoundPlay soundPlay;

    [SerializeField]
    private bool loop;

    [SerializeField]
    private float delay;

    [SerializeField]
    private TimerCount interval;

    [SerializeField]
    private float distanceMax;

    public void ExecuteAction(float distance)
    {
        if (distance > distanceMax)
            return;

        if (interval.TimerOk())
            StartCoroutine(soundPlay.CorPlay(clip, loop, delay));
    }
}
