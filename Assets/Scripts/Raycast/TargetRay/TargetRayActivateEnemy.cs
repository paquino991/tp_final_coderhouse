﻿using UnityEngine;

public class TargetRayActivateEnemy : MonoBehaviour, ITargetRayPlayer
{
    [SerializeField]
    private Enemy enemy;

    [SerializeField]
    private float distanceMax;

    public void ExecuteAction(float distance)
    {
        if (distance < distanceMax)
        {
            enemy.ChangeMovementEnemy(MovementNpcEnum.WALK);
        }
    }

}
