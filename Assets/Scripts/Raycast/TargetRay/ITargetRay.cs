﻿public interface ITargetRayPlayer
{
    void ExecuteAction(float distance);
}
