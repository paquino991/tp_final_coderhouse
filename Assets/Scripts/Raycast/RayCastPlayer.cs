﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastPlayer : MonoBehaviour
{
    [SerializeField]
    private RayCastSome rayCast;

    [SerializeField]
    private float distanceMax;

    [SerializeField]
    private LayerMask layers;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CreateRayCast();
    }

    private void CreateRayCast()
    {
        var ray = rayCast.GetRayCast<ITargetRayPlayer>(distanceMax, layers, out float distance);
        if (ray != null)
        {
            foreach(var r in ray)
            {
                r.ExecuteAction(distance);
            }
        }
    }
}
