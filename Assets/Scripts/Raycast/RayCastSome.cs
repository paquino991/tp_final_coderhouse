﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RayCastSome : MonoBehaviour
{
    [SerializeField]
    private Transform[] visions;

    [SerializeField]
    private float distanceGizmos;

    [SerializeField]
    private float radius;

    public List<T> GetRayCast<T>(float distanceMax, LayerMask layers, out float distance)
    {
        for (int i = 0; i < visions.Length; i++)
        {
            
            if (!Physics.SphereCast(visions[i].position, radius, visions[i].forward, out RaycastHit raycastHit, distanceMax, layers))
                continue;

            if (raycastHit.rigidbody == null)
                continue;

            var targetRay = raycastHit.collider.GetComponents<T>();

            if (targetRay != null)
            {
                distance = raycastHit.distance;
                return targetRay.ToList();
            }
                
        }

        distance = 0;
        return null;
    }

    //public T GetRayCast<T>(float distanceMax, LayerMask layers)
    //{
    //    for (int i = 0; i < visions.Length; i ++)
    //    {
    //        if (!Physics.Raycast(visions[i].position, visions[i].forward, out RaycastHit raycastHit, distanceMax, layers))
    //            break;

    //        if (raycastHit.rigidbody == null)
    //            break;

    //        var targetRay = raycastHit.collider.GetComponent<T>();

    //        if (targetRay != null)
    //            return targetRay;
    //    }

    //    return default(T);
    //}

    private void OnDrawGizmos()
    {
        for (int i = 0; i < visions.Length; i++)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(visions[i].position, visions[i].forward * distanceGizmos);
        }
    }
}
