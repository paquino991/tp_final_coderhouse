﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static Vector3 RandomVector(this Vector3 v, int min, int max)
    {
        short multiplactor = 1;
        if (Random.Range(0, 2) == 0)
            multiplactor = -1;

        float x = Random.Range(min, max) * 0.01f * multiplactor;
        float z = Random.Range(min, max) * 0.01f * multiplactor;

        return new Vector3(v.x + x, v.y, v.z + z);
    }
}
