﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class LightEffect : MonoBehaviour
{
    public float min;
    public float max;

    public Light light;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        light.intensity =  min + (Mathf.PingPong(Time.time, 1) * (max - min));
    }
}
