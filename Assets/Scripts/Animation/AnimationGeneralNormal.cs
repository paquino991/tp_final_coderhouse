﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationGeneralNormal : AnimationStrategy
{

    public override void Hit()
    {
        this.anim.SetTrigger("Hit");
    }

    public override void Hit2()
    {
        this.anim.SetTrigger("Hit2");
    }

    public override void Iddle()
    {
        this.anim.SetBool("Walk", false);
        this.anim.SetBool("Run", false);
    }

    public override void Run()
    {
        this.anim.SetBool("Walk", false);
        this.anim.SetBool("Run", true);
    }

    public override void Walk()
    {
        this.anim.SetBool("Walk", true);
        this.anim.SetBool("Run", false);
    }

    public override void Die()
    {
        this.anim.SetTrigger("Die");
    }

    public override void Equip()
    {
        this.anim.SetBool("Equip", true);
    }

    public override void UnEquip()
    {
        this.anim.SetBool("Equip", false);
    }
}
