﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimationStrategy : MonoBehaviour
{
    [SerializeField]
    protected Animator anim;

    public abstract void Walk();
    public abstract void Run();
    public abstract void Iddle();
    public abstract void Hit();
    public abstract void Hit2();
    public abstract void Die();
    public abstract void Equip();
    public abstract void UnEquip();

}
