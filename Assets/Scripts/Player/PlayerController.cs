﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Stats
{
    [SerializeField]
    public float staMax;
    [SerializeField]

    public float staMin;

    [SerializeField]

    public float percentageEnergyRun;

}

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speedRotate;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float speedRun;

    [SerializeField]
    private Transform lookUp;

    [SerializeField]
    private float maxRotateUp;

    [SerializeField]
    private CinemachineVirtualCamera cinemachine;

    [SerializeField]
    private CinemachineHardLockToTarget cinemachineFramingTransposer;

    [SerializeField]
    private AudioSource soundWalk;

    [SerializeField]
    private AudioSource soundEnergyDown;

    [SerializeField]
    private AnimationStrategy anim;

    [SerializeField]
    public Transform transformItemEquip;

    [SerializeField]
    public ItemEquip itemEquip;

    [SerializeField]
    public Inventory inventory;

    [SerializeField]
    public TimerCount intervalKeyEquip;

    [SerializeField]
    public TimerCount intervalRestoreEnergy;

    [SerializeField]
    public Stats stats;

    private float rotateTotal;

    private bool rotateUpActived;

    public static event Action OnMovement;
    public static event Action OnRotate;

    private void Start()
    {
        StartCoroutine(ActiveRotateUp());
        this.cinemachineFramingTransposer = cinemachine.GetCinemachineComponent<CinemachineHardLockToTarget>();

        Cursor.visible = false;
    }

    private void Update()
    {
        if (ManagerGame.Instance.IsPaused)
            return;

        PlaySoundEnergyDown();
        RestoreEnergy();
        CheckKeyboard();
    }

    public AnimationStrategy GetAnimation()
    {
        return this.anim;
    }

    //Todas las acciones por teclado
    private void CheckKeyboard()
    {
        Rotate();
        Move();
        UseInventory();
    }

    private void UseInventory()
    {
        if (ManagerKeyboard.Instance.IsKeyPress(KeyboardPlayerEnum.EquipObject) || Input.GetMouseButtonDown(1))
            if(intervalKeyEquip.TimerOk())
                this.inventory.GetById(0).Use();
    }

    private void Rotate()
    {
        var x = Input.GetAxis("Mouse X");
        transform.Rotate(Vector3.up * x * speedRotate * Time.deltaTime);
        RotateUp();


        //cinemachineFramingTransposer.m_ScreenY += y * speedRotate;
        //if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        //    transform.Rotate(Vector3.up * speedRotate * Time.deltaTime);
        //else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        //    transform.Rotate(-Vector3.up * speedRotate * Time.deltaTime);
    }

    private void RotateUp()
    {
        if (!rotateUpActived)
            return;

        var y = Input.GetAxis("Mouse Y");

        float rotateTemp = rotateTotal + y;

        if (maxRotateUp < System.Math.Abs(rotateTemp))
            return;

        rotateTotal = rotateTemp;
        lookUp.Rotate(Vector3.left * y * speedRotate * Time.deltaTime);
        OnRotate?.Invoke();
        ///lookUp.rotation.SetEulerAngles(new Vector3(lookUp.rotation.eulerAngles.x + y * speedRotate * Time.deltaTime, lookUp.rotation.eulerAngles.x + y * speedRotate * Time.deltaTime * 0.1f, lookUp.rotation.eulerAngles.z));
    }

    private void Move()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        float velocity = speed;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            ConsumeEnergy(1);
            float difVelocity = speedRun - speed;
            velocity = CalculateVelocity();
        }

        if (vertical > 0)
            transform.position += transform.forward * velocity * Time.deltaTime;
        else if (vertical < 0)
            transform.position -= transform.forward * velocity * Time.deltaTime;

        if (horizontal > 0)
            transform.position += transform.right * velocity * Time.deltaTime;
        else if (horizontal < 0)
            transform.position -= transform.right * velocity * Time.deltaTime;

        if (vertical == 0 && horizontal == 0)
        {
            anim.Iddle();

            if (soundWalk.isPlaying)
                soundWalk.Stop();
        }
        else
        {
            OnMovement?.Invoke();

            anim.Walk();

            if (!soundWalk.isPlaying)
                soundWalk.Play();
        }



    }

    private IEnumerator ActiveRotateUp()
    {
        yield return new WaitForSeconds(1);
        rotateUpActived = true;
    }

    private void RestoreEnergy()
    {
        if (this.intervalRestoreEnergy.TimerOk() && this.stats.staMax > this.stats.staMin)
            this.stats.staMin += 1;
    }

    private void ConsumeEnergy(int points)
    {
        if (this.stats.staMin > 0)
            this.stats.staMin -= 1;
    }

    private float CalculateVelocity()
    {
        float difVelocity = speedRun - speed;
        float percentageEnergy = this.stats.staMin / this.stats.staMax;

        if (this.stats.percentageEnergyRun < percentageEnergy)
            return speedRun;
        else
            return speed + (difVelocity * this.stats.staMin / this.stats.staMax);
    }

    private void PlaySoundEnergyDown()
    {
        float percentageEnergy = this.stats.staMin / this.stats.staMax;

        if (this.stats.percentageEnergyRun > percentageEnergy && !this.soundEnergyDown.isPlaying)
            this.soundEnergyDown.Play();
        else if (this.soundEnergyDown.isPlaying && this.stats.percentageEnergyRun <= percentageEnergy)
            this.soundEnergyDown.Stop();
    }
}
