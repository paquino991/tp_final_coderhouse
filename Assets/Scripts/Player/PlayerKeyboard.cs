﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyboardPlayerEnum
{
    EquipObject,
    OpenMenu
}

public class ManagerKeyboard : MonoBehaviour
{
    private static ManagerKeyboard _instance { get; set; }

    public static ManagerKeyboard Instance
    {
        get
        {
            if (_instance == null)
                _instance = new ManagerKeyboard();

            return _instance;
        }
    }

    public ManagerKeyboard()
    {
        Dic = new Dictionary<KeyboardPlayerEnum, KeyCode>
        {
            { KeyboardPlayerEnum.EquipObject, KeyCode.E },
            { KeyboardPlayerEnum.OpenMenu, KeyCode.Escape }
        };
    }

    public Dictionary<KeyboardPlayerEnum, KeyCode> Dic { get; set; }

    public KeyCode GetKeyCodeByKey(KeyboardPlayerEnum key)
    {
        return Dic[key];
    }

    public bool IsKeyPress(KeyboardPlayerEnum key)
    {
        return Input.GetKey(GetKeyCodeByKey(key));
    }
}
