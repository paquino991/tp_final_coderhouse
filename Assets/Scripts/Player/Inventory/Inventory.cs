﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Inventory : MonoBehaviour
{

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private short maxSlots;

    [SerializeField]
    private List<Slot> slots;

    public void Equiped(int id)
    {
        slots[id].Use();
    }

    private void Start()
    {
        InitializeInventory();
    }

    private void InitializeInventory()
    {
        for (int i = 0; i < maxSlots; i ++)
        {
            slots.Add(new Slot(player, (short)i));
        }
    }

    public Slot GetById(short id)
    {
        return slots.FirstOrDefault(s => s.GetId() == id);
    }
}
