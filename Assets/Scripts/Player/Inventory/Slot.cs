﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Slot
{
    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private short idSlot;

    [SerializeField]
    private Item item;

    public Slot(PlayerController player, short idSlot)
    {
        this.player = player;
        this.idSlot = idSlot;
    }

    public void Use()
    {
        item.Use(player);
    }

    public short GetId()
    {
        return idSlot;
    }

}
