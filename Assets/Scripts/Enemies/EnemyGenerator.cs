﻿using System.Linq;
using UnityEngine;
using Assets.Scripts.Helpers;
using UnityEngine.Events;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField]
    private Transform[] positions;

    [SerializeField]
    private Enemy[] enemies;

    [SerializeField]
    public UnityEvent OnCreateEnemy;
    // Start is called before the first frame update
    void Start()
    {
        GenerateFromLast();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GenerateFromFirst()
    {
        if (enemies.Length > positions.Length)
            throw new System.Exception("La cantidad de posiciones debe ser igual o superior a la cantidad de enemigos a instanciar");

        var listPositions = positions.ToList().OrderRandom();

        for (int i = 0; i < enemies.Length - 1; i++)
        {
            CreateEnemy(enemies[i], listPositions[i].position);
        }
    }

    private void GenerateFromLast()
    {
        if (enemies.Length > positions.Length)
            throw new System.Exception("La cantidad de posiciones debe ser igual o superior a la cantidad de enemigos a instanciar");

        var listPositions = positions.ToList().OrderRandom();

        for (int i = enemies.Length - 1; i >= 0; i--)
        {
            CreateEnemy(enemies[i], listPositions[i].position);
        }
    }


    private void CreateEnemy(Enemy enemy, Vector3 position)
    {
        var gm = Instantiate(enemy.gameObject);
        gm.transform.position = new Vector3(position.x, position.y, position.z);
        gm.transform.parent = this.transform;
        gm.SetActive(true);
        OnCreateEnemy.Invoke();
    }
}
