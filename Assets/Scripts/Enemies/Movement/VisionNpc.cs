﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class VisionNpc
{
    private List<Enemy> EnemiesSeePlayer { get; set; }

    private static VisionNpc _Instance { get; set; }
    public static VisionNpc Instance { get {
            if (Instance == null)
                _Instance = new VisionNpc();

            return _Instance;
        } }

    public VisionNpc()
    {
        this.EnemiesSeePlayer = new List<Enemy>();
    }

    public void Add(Enemy enemy)
    {
        if(!EnemiesSeePlayer.Any(e=>e==enemy))
            EnemiesSeePlayer.Add(enemy);
    }

    public void Remove(Enemy enemy)
    {
        if (EnemiesSeePlayer.Any(e => e == enemy))
            EnemiesSeePlayer.Remove(enemy);
    }

    public bool SomeEnemieSeePlayer()
    {
        return this.EnemiesSeePlayer.Count() > 0;
    }
}
