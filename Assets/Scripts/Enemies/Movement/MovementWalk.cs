﻿using UnityEngine;

public class MovementWalk : MovementEnemyStrategy
{
    private float minDistance { get; set; }
    public MovementWalk(AnimationStrategy animationStrategy, Transform myTransform, Transform targetTransform, float speed, float maxDistance, float minDistance) : base(animationStrategy, myTransform, targetTransform, speed, maxDistance) {
        this.minDistance = minDistance;
    }
    public override void Persecute()
    {
        if (!ValidateDistance())
        {
            this.animationStrategy.Iddle();
            return;
        }
            

        if (this.minDistance > Vector3.Distance(myTransform.position, targetTransform.position))
        {
            this.animationStrategy.Iddle();
            return;
        }
            
        this.animationStrategy.Walk();
        var rotation = Quaternion.LookRotation(new Vector3(targetTransform.position.x, 0, targetTransform.position.z) - new Vector3(myTransform.position.x, 0, myTransform.position.z));
        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, rotation, speed);
        myTransform.position += myTransform.forward * Time.deltaTime * speed * 1.0f;
    }
}


