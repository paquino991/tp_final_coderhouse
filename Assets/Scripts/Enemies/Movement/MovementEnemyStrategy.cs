﻿using UnityEngine;

public abstract class MovementEnemyStrategy
{
    protected readonly Transform myTransform;
    protected readonly Transform targetTransform;
    protected float speed { get; set; }
    protected float maxDistance { get; set; }

    protected AnimationStrategy animationStrategy { get; set; }
    public MovementEnemyStrategy(AnimationStrategy animationStrategy, Transform myTransform, Transform targetTransform, float speed, float maxDistance)
    {
        this.myTransform = myTransform;
        this.targetTransform = targetTransform;
        this.speed = speed;
        this.maxDistance = maxDistance;
        this.animationStrategy = animationStrategy;
    }
    public abstract void Persecute();

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetMaxDistance(float maxDistance)
    {
        this.maxDistance = maxDistance;
    }

    protected bool ValidateDistance()
    {
        return maxDistance == 0 || Vector3.Distance(this.myTransform.position, this.targetTransform.position) <= maxDistance;
    }
}
