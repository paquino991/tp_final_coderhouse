﻿using UnityEngine;

public class MovementRotation : MovementEnemyStrategy
{
    public MovementRotation(AnimationStrategy animationStrategy, Transform myTransform, Transform targetTransform, float speed, float maxDistance) : base (animationStrategy, myTransform, targetTransform, speed, maxDistance) { }
    public override void Persecute()
    {
        this.animationStrategy.Iddle();

        if (!ValidateDistance())
            return;

        var rotation = Quaternion.LookRotation(targetTransform.position - myTransform.position);
        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, rotation, speed * Time.deltaTime);
    }
}
