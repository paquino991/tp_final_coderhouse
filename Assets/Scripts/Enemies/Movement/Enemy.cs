﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    [SerializeField]
    private float energyRunMin;

    [SerializeField]
    private EnemyData enemyData;

    [SerializeField]
    private MovementNpcEnum movementEnemy;

    private MovementNpcEnum? _movementEnemy;

    private MovementEnemyStrategy movement;

    [SerializeField]
    private AnimationGeneralNormal animation;

    [SerializeField]
    private TimerCount timerEnergyCost;

    public event Action OnUpdateMovementType;

    void Start()
    {
        ChangeMovementEnemy(movementEnemy);
    }

    // Update is called once per frame
    void Update()
    {
        if (ManagerGame.Instance.IsPaused)
            return;

        //ChangeMovementEnemy(movementEnemy);
        UpdateEnergy();
        UpdateVelocity();
        UpdateTypeMovement();
        this.movement.Persecute();
    }
    public void ChangeMovementEnemy(MovementNpcEnum movementEnemy)
    {
        if (this._movementEnemy != movementEnemy)
        {
            this._movementEnemy = movementEnemy;
            this.movement = FactoryMovementEnemy.Create(this.animation, (MovementNpcEnum)_movementEnemy, this.transform, this.target, this.energyRunMin > 0 ? this.enemyData.SpeedRun : this.enemyData.Speed, this.enemyData.MaxDistance, this.enemyData.MinDistance);
            OnUpdateMovementType?.Invoke();
        }
    }

    private void UpdateTypeMovement()
    {
        if (!ValidateDistance())
        {
            this.energyRunMin = this.enemyData.EnergyRunMax; //Reseteamos la energia del NPC
            ChangeMovementEnemy(MovementNpcEnum.ROTATION);
            return;
        }
    }

    protected bool ValidateDistance()
    {
        return this.enemyData.MaxDistance == 0 || Vector3.Distance(this.transform.position, this.target.position) <= this.enemyData.MaxDistance;
    }

    private void UpdateEnergy()
    {
        if (this.timerEnergyCost.TimerOk() && this.energyRunMin > 0 && this._movementEnemy == MovementNpcEnum.WALK)
        {
            this.energyRunMin -= 1;
        }
    }

    private void UpdateVelocity()
    {
        if (this.energyRunMin <= 0)
            this.movement.SetSpeed(this.enemyData.Speed);
    }


}
