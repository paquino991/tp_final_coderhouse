﻿using UnityEngine;

public enum MovementNpcEnum
{
    ROTATION,
    WALK,
    RUN,
    WALK_DEATH
}

public static class FactoryMovementEnemy
{
    public static MovementEnemyStrategy Create(AnimationStrategy animationStrategy, MovementNpcEnum movementEnemyEnum, Transform myTransform, Transform targetTransform, float speed, float maxDistance, float minDistance)
    {
        switch (movementEnemyEnum)
        {
            case MovementNpcEnum.ROTATION: return new MovementRotation(animationStrategy, myTransform, targetTransform, speed, maxDistance); break;
            case MovementNpcEnum.WALK: return new MovementWalk (animationStrategy, myTransform, targetTransform, speed, maxDistance, minDistance); break;
            //case MovementNpcEnum.WALK_DEATH: return new MovementWalkDeath(animationStrategy, myTransform, targetTransform, speed, maxDistance, minDistance); break;
            default: Debug.LogError("Error Tipo de movimiento desconocido"); return null;
        }
    }
}