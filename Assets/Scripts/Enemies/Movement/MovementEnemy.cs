﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementEnemy : MonoBehaviour
{
    [SerializeField]
    private float maxDistance;

    [SerializeField]
    private float minDistance;

    [SerializeField]
    private Transform target;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float speedRun;

    [SerializeField]
    private float energyRunMin;

    [SerializeField]
    private float energyRunMax;

    [SerializeField]
    private MovementNpcEnum movementEnemy;

    private MovementNpcEnum? _movementEnemy;

    private MovementEnemyStrategy movement;

    [SerializeField]
    private AnimationGeneralNormal animation;

    [SerializeField]
    private TimerCount timerEnergyCost;

    void Start()
    {
        ChangeMovementEnemy(movementEnemy);
    }

    // Update is called once per frame
    void Update()
    {
        if (ManagerGame.Instance.IsPaused)
            return;

        //ChangeMovementEnemy(movementEnemy);
        UpdateEnergy();
        UpdateVelocity();
        UpdateTypeMovement();
        this.movement.Persecute();
    }
    public void ChangeMovementEnemy(MovementNpcEnum movementEnemy)
    {
        if (this._movementEnemy != movementEnemy)
        {
            this._movementEnemy = movementEnemy;
            this.movement = FactoryMovementEnemy.Create(this.animation, (MovementNpcEnum)_movementEnemy, this.transform, this.target, this.energyRunMin > 0 ? this.speedRun : this.speed, this.maxDistance, this.minDistance);
        }
    }

    private void UpdateTypeMovement()
    {
        if (!ValidateDistance())
        {
            this.energyRunMin = energyRunMax; //Reseteamos la energia del NPC
            ChangeMovementEnemy(MovementNpcEnum.ROTATION);
            return;
        }
    }

    protected bool ValidateDistance()
    {
        return maxDistance == 0 || Vector3.Distance(this.transform.position, this.target.position) <= maxDistance;
    }

    private void UpdateEnergy()
    {
        if (this.timerEnergyCost.TimerOk() && this.energyRunMin > 0 && this._movementEnemy == MovementNpcEnum.WALK)
        {
            this.energyRunMin -= 1;
        }
    }

    private void UpdateVelocity()
    {
        if (this.energyRunMin <= 0)
            this.movement.SetSpeed(this.speed);
    }

    //private void ExecutePersecuteGlobal()
    //{
    //    if (Vector3.Distance(this.transform.position, this.target.position) > maxDistance)
    //    {
    //        VisionNpc.Instance.Remove(this);
    //        return;
    //    }

    //    VisionNpc.Instance.Add(this);
    //}

}
