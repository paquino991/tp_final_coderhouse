﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/EnemyData")]
public class EnemyData : ScriptableObject
{
    [SerializeField]
    private float maxDistance;

    [SerializeField]
    private float minDistance;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float speedRun;

    [SerializeField]
    private float energyRunMin;

    [SerializeField]
    private float energyRunMax;

    public float MaxDistance { get { return maxDistance; } }
    public float MinDistance { get { return minDistance; } }
    public float Speed { get { return speed; } }
    public float SpeedRun { get { return speedRun; } }
    public float EnergyRunMin { get { return energyRunMin; } }
    public float EnergyRunMax { get { return energyRunMax; } }

}
